const express = require("express");
const cors = require("cors");
const { createQuiz } = require("./lib/createQuiz");
const { connectDb } = require("./lib/DB/index");
const app = express();
const port = 3000;

// Load environment variables from .env file
require("dotenv").config();

// Connect to MongoDB
connectDb().catch(console.error);

// Enable CORS for all origins
app.use(cors());

// Disable CORS pre-flight checks for all routes
app.options("*", cors());

app.get("/quiz", async (req, res) => {
  const url = req.query.url;
  if (url) {
    const quiz = await createQuiz(url);
    if (quiz == null) {
      res.status(400).json({ error: "No article found" });
    } else {
      res.json(quiz);
    }
  } else {
    res.status(400).json({ error: "No URL specified" });
  }
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
