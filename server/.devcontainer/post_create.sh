#!/bin/bash
sudo apt-get -y install redis nginx
sudo cp -r nginx/* /etc/nginx/
cd src
bundle install

sudo mkdir /var/punch
sudo chown $(whoami):$(whoami) /var/punch
