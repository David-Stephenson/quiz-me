const { MongoClient } = require("mongodb");

const uri = "mongodb://localhost:27017";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

let database = null;

async function connectDb() {
  try {
    await client.connect();
    console.log("Connected successfully to MongoDB server");
    database = client.db("mydb");
  } catch (error) {
    console.error("Error connecting to MongoDB", error);
  }
}

async function storeArticle(url, html, plainText) {
  const article = { url, html, plainText };
  const articles = database.collection("articles");
  const result = await articles.updateOne({ url: url }, { $set: article }, { upsert: true });
  return result;
}

async function getArticle(url) {
  const articles = database.collection("articles");
  const article = await articles.findOne({ url: url });
  return article;
}

async function storeQuiz(url, data) {
  const quiz = { url, data };
  const quizzes = database.collection("quizzes");
  const result = await quizzes.updateOne({ url: url }, { $set: quiz }, { upsert: true });
  return result;
}

async function getQuiz(url) {
  const quizzes = database.collection("quizzes");
  const quiz = await quizzes.findOne({ url: url });
  return quiz;
}

module.exports = { connectDb, storeArticle, getArticle, storeQuiz, getQuiz };
