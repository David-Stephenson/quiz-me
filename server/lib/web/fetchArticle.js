const axios = require("axios");
const cheerio = require("cheerio");
const Parser = require("@postlight/parser");
const he = require("he");
const { storeArticle, getArticle } = require("../DB/index");

async function fetchArticle(targetURL) {
  // Check if the article is already in the database.
  const existingArticle = await getArticle(targetURL);
  if (existingArticle) {
    return existingArticle;
  }

  const url = "http://localhost:3001/scrape";
  const body = {
    url: targetURL,
  };

  try {
    const response = await axios({
      method: "get",
      url: url,
      data: body,
      headers: {
        "Content-Type": "application/json",
      },
    });

    let html = response.data.content;
    const $ = cheerio.load(html);

    // If the scraped HTML doesn't contain <article> tag, return null
    if ($("article").length === 0) {
      console.error("No article found");
      return null;
    }

    const article = await Parser.parse(targetURL, { html });

    if (article && article.content && article.content.trim().length > 0) {
      const $cleaned = cheerio.load(article.content);

      $cleaned("*").each(function () {
        $cleaned(this).removeAttr("class");
      }); // Remove all classes

      $cleaned("img").each(function () {
        if (!$(this).parent().is("center")) {
          $(this).wrap("<center></center>");
        }
        $(this).addClass("p-4"); // Add tailwind CSS padding to images
      });

      $cleaned("a").attr("target", "_blank"); // Open all links in new tabs

      let cleanedHTML = $cleaned.html();
      cleanedHTML = cleanedHTML.replace(/\n/g, ""); // Remove all newline characters

      let textOnly = cleanedHTML;
      textOnly = he.decode(textOnly);
      textOnly = textOnly.replace(/<\/?[^>]+(>|$)/g, ""); // Remove all HTML tags
      textOnly = textOnly.replace(/<[^>]*\/\s?>/g, ""); // Remove all self-closing tags
      textOnly = textOnly.replace(/\t/g, ""); // Remove all tab characters
      textOnly = textOnly.replace(/‘|’|‛|‚|′|`/g, "'");
      textOnly = textOnly.replace(/“|”|‟|„|″|❝|❞/g, '"');
      textOnly = textOnly.replace(/(\.|,|;|:|!|\?)\s/g, "$1");
      textOnly = textOnly.replace(/\s("|')/g, "$1");
      textOnly = textOnly.replace(/("|')\s/g, "$1");
      textOnly = textOnly.replace(/—/g, "-");
      textOnly = textOnly.replace(/\s\s+/g, " ");
      textOnly = textOnly.trim();

      // Storing the article into the database.
      await storeArticle(targetURL, cleanedHTML, textOnly);

      return { html: cleanedHTML, plainText: textOnly };
    } else {
      console.error("No content found");
      return null;
    }
  } catch (error) {
    console.error(error);
    return null;
  }
}

module.exports = { fetchArticle };
