const { Configuration, OpenAIApi } = require("openai");
const { storeQuiz, getQuiz } = require("../DB/index");
const configuration = new Configuration({
  apiKey: "not today buddy :))",
});

const openai = new OpenAIApi(configuration);

async function fetchQuiz(articleUrl, articleText) {
  const existingQuiz = await getQuiz(articleUrl);
  if (existingQuiz) {
    return existingQuiz;
  } else {
    console.log("FAIL");
  }

  const chatCompletion = await openai.createChatCompletion({
    model: "gpt-4",
    messages: [
      {
        role: "system",
        content: `Write a 10 question quiz based on the main information from an article provided, write the quiz data in a json array like [{"q":"", o:[], a:index]`,
      },
      {
        role: "user",
        content: articleText,
      },
    ],
  });

  quiz = JSON.parse(chatCompletion.data.choices[0].message.content);

  // Storing the quiz into the database.
  await storeQuiz(articleUrl, quiz);

  return quiz;
}

module.exports = { fetchQuiz };
