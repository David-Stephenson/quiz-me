const { fetchArticle } = require("./web/fetchArticle");
const { fetchQuiz } = require("./web/fetchQuiz");

async function createQuiz(url) {
  const articleData = await fetchArticle(url);

  if (articleData == null) {
    return null;
  }

  const quizData = await fetchQuiz(url, articleData.plainText);

  console.log(quizData);

  return { article: articleData.html, quiz: quizData.data || quizData };
}

module.exports = { createQuiz };
