import { toastStore } from "../stores";

const createToast = () => {
  const { subscribe, update } = toastStore;

  return {
    subscribe,
    add: (message, type = "info") => update((all) => [...all, { id: Date.now(), message, type }]),
    remove: (id) => update((all) => all.filter((m) => m.id !== id)),
  };
};

export const toast = createToast();
