import { writable } from "svelte/store";

const createModal = () => {
  const { subscribe, set, update } = writable({ title: "", message: "", isOpen: false });

  let resolveOpen = null;

  const openModal = (title, message) => {
    set({ title, message, isOpen: true });
    return new Promise((resolve) => {
      resolveOpen = resolve;
    });
  };

  const closeModal = () => {
    update((state) => ({ ...state, isOpen: false }));
  };

  return {
    subscribe,
    openModal,
    closeModal,
    resolveOpen,
  };
};

export const modal = createModal();
