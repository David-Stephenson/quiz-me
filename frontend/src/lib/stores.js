import { writable } from "svelte/store";

export const urlStore = writable("");
export const toastStore = writable([]);
export const modalStore = writable({});
